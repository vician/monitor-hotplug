#!/bin/sh
# xrandr \
# --output HDMI3 --primary --mode 1920x1200 --pos 0x0 --rotate normal \
# --output LVDS1 --mode 1366x768 --pos 1920x432 --rotate normal \
# --output VGA1 --off --output VIRTUAL1 --off --output DP3 --off --output DP2 --off --output DP1 --off  --output HDMI2 --off --output HDMI1 --off 

if [ $# -eq 0 ]; then
	echo "Wrong usage!"
	echo "$0 PRIMARY_OUTPUT [SECONDARY_OUTPUT1] [SECONDARY_OUTPUT2] [...]"
	exit 0
fi

primary=""
x=0
max_height=0
xrandr="xrandr"

for output in $@; do
	echo "- $output"
	xrandr | grep " connected" | grep -q $output 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo -e "\tnot connected - skipping"
		continue
	fi
	# Get resolution
	resolution=$(xrandr -q | grep -A 1 -C 0 $output | grep -v $output | awk '{print $1}')
	width=$(echo $resolution | awk -F'x' '{print $1}')
	height=$(echo $resolution | awk -F'x' '{print $2}')
	echo -e "\tResolution: $width x $height"

	xrandr="$xrandr --output $output"
	if [ -z "$primary" ]; then
		xrandr="$xrandr --primary"
		primary=$output
	fi
	if [ $height -gt $max_height ]; then
		max_height=$height
	fi
	y=$(expr $max_height - $height)
	xrandr="$xrandr --mode ${width}x${height} --pos ${x}x${y} --rotate normal"
	x=$(expr $x + $width)
done

echo "---"

# Find not specified outputs and disable them
for output in $(xrandr -q | grep "connected" | awk '{print $1}'); do
	echo "- $output"
	echo $* | grep -q $output 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		xrandr | grep " connected" | grep -q $output 1>/dev/null 2>/dev/null
		if [ $? -eq 0 ]; then
			echo -e "\t already used - skipping"
			continue
		fi
	fi
	xrandr="$xrandr --output $output --off"
done

echo "---"

echo $xrandr
$xrandr
