# monitor-hotplug

Simple script for display configuration. Based on udev which starts script.

## Init

```
git clone https://github.com/vician/monitor-hotplug
cd monitor-hotplug
./init.sh
```
You will be asked for sudo password and default monitor configuration.

## Usage

Udev automatically runs `./monitor-hotplug.sh` which use default monitor configuration and apply it.

It skips missing outputs.

### Manual start

Run `./monitor-hotplug.sh`

## Knonw issues

* Udev sometimes not run (monitored by `udevadm monitor | grep drm`) -> maybe it's time to use systemd-udevd.
