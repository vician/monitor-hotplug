#!/bin/bash

exec >> /tmp/monitor-hotplug.log 2>&1

base_dir=$(dirname $0)
base_dir=$(realpath $base_dir)

xrandr="$base_dir/xrandr-auto.sh"

primary_file="$base_dir/primary"
if [ ! -f $primary_file ] || [ ! -s $primary_file ]; then
	echo "Not found or empty primary file ($primary_file), please init first!"
	echo "run: $base_dir/init.sh"
	exit 0
fi

primary=$(cat $primary_file)

xauthority_config="$base_dir/xauthority"
if [ ! -f $xauthority_config ] || [ ! -s $xauthority_config ]; then
	echo "Not found or empty xauthority config file ($xauthority_config), please init first!"
	echo "run: $base_dir/init.sh"
	exit 0
fi

xauthority=$(cat $xauthority_config)

export DISPLAY=:0
export XAUTHORITY=$xauthority

secondary=""
for output in $(xrandr -q | grep " connected"| awk '{print $1}'); do
	echo "- $output"
	echo $primary | grep -q $output 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		echo -e "\t- in primary - skipping"
		continue
	fi
	secondary="$secondary $output"
done

echo "Outputs: $primary$secondary"

$xrandr $primary$secondary

i3-msg reload
