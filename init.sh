#!/bin/bash

base_dir=$(dirname $0)
base_dir=$(realpath $base_dir)

udev_file="/etc/udev/rules.d/80-monitor-hotplug.rules"

script="$base_dir/monitor-hotplug.sh"

xauthority="$HOME/.Xauthority"
echo "Enabling it for user $USER with home $HOME"
if [ ! -f $xauthhority ]; then
	echo "Xautorhority file not found! Do you use X server? Exiting."
	exit 0
fi

xauthority_config="$base_dir/xauthority"
echo $xauthority > $xauthority_config

# Does the udev file exist?
if [ -f $udev_file ]; then
	echo "Udev file already exist. If you want to reinit, please remove it first."
	echo "sudo rm $udev_file"
else
	# Create the udev file
	echo -e 'SUBSYSTEM=="drm", ACTION=="change", RUN+="'$script'"' | sudo tee $udev_file
	if [ $? -ne 0 ]; then
		echo "ERROR: Cannot write udev rule!!"
		exit 1
	fi

	# Reload the udevadm
	sudo udevadm control --reload-rules
	if [ $? -ne 0 ]; then
		echo "ERROR: Cannot reload udevadm!"
		exit 1
	fi
fi



primary_file="$base_dir/primary"

echo "Please select your primary output(s), more outputs separate by space"
if [ -f $primary_file ] && [ -s $primary_file ]; then
	echo "Current configuration: '$(cat $primary_file)'"
fi
echo "e.q.: $(xrandr -q | grep " connected" | awk '{print $1}' | sed ':a;N;$!ba;s/\n/ /g')"
read outputs
echo "You selected '$outputs' as primary output"
if [ -z "$outputs" ]; then
	echo "Empty string - exiting!"
else
	echo $outputs > $primary_file
fi
